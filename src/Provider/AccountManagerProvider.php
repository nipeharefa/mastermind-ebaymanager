<?php

namespace MasterMind\Component\EbayManager\Provider;

use Illuminate\Support\ServiceProvider;
use MasterMind\Component\EbayManager\Manager\EbayAccountInterface;
use MasterMind\Component\Contracts\EbayShippingServiceInterface;
use MasterMind\Component\EbayManager\Manager\AccountManager;
use MasterMind\Component\EbayManager\Manager\AccountManagerInterface;

class AccountManagerProvider extends ServiceProvider
{
    public function boot()
    {
        $this->registerAccountManager();
    }

    public function register()
    {
    }

    private function registerAccountManager()
    {
        $this->app->singleton(AccountManager::class, function($app) {
            return new AccountManager(
                $app->get(EbayShippingServiceInterface::class)
            );
        });

        $this->app->bind(AccountManagerInterface::class, AccountManager::class);
    }
}