<?php

namespace MasterMind\Component\EbayManager\Manager;

use Illuminate\Database\Eloquent\Collection;
use MasterMind\Component\Contracts\EbayAccountInterface;
use MasterMind\Component\Contracts\EbayShippingServiceInterface;

class AccountManager implements AccountManagerInterface
{
    private $ebayShippingService;

    /**
     *
     * @param EbayShippingServiceInterface $ebayShippingService
     */
    public function __construct(EbayShippingServiceInterface $ebayShippingService)
    {
        $this->ebayShippingService = $ebayShippingService;
    }

    /**
     *
     * @param EbayAccountInterface $account
     * @return Collection
     */
    public function getShippingDomestic(EbayAccountInterface $account): Collection
    {
        return $this
            ->ebayShippingService
            ->getShippingDomestic($account)
        ;
    }

    /**
     * 
     * @param EbayACcountInterface $account
     * @return array
     */
    public function getDataForSettingView(EbayAccountInterface $account): array
    {
        return [
            'shippingDomestic' => $this->getShippingDomestic($account),
        ];
    }
}