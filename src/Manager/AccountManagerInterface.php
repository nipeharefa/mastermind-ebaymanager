<?php

namespace MasterMind\Component\EbayManager\Manager;

use MasterMind\Component\Contracts\EbayAccountInterface;

interface AccountManagerInterface
{
    public function getDataForSettingView(EbayAccountInterface $ebayAccount): array;
}