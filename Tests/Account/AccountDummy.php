<?php

namespace MasterMind\Component\EbayManager\Test\Account;

use MasterMind\Component\Contracts\EbayAccountInterface;

class AccountDummy implements EbayAccountInterface
{
    public function getSiteId()
    {
        return "usa";
    }
}