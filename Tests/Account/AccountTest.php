<?php
namespace MasterMind\Component\EbayManager\Test\Account;

use Illuminate\Database\Eloquent\Collection;
use MasterMind\Component\EbayManager\Manager\AccountManager;
use MasterMind\Component\Contracts\EbayAccountInterface;
use MasterMind\Component\EbayManager\Manager\AccountManagerInterface;
use MasterMind\Component\Contracts\EbayShippingServiceInterface;
use MasterMind\Component\EbayManager\Test\TestCase;

class AccountTest extends TestCase
{
    public function testGetShippingDomestic()
    {
        $item = $this->getMockBuilder(AccountManager::class)
            ->disableOriginalConstructor()
            ->setMethods(['getShippingDomestic'])
            ->getMock()
        ;

        $collection = $this->getMockBuilder(Collection::class)->getMock();
        $item->method('getShippingDomestic')
            ->with($this->isInstanceOf(EbayAccountInterface::class))
            ->will($this->returnValue($collection))
        ;

        $dummy = new AccountDummy();
        $value = $item->getShippingDomestic($dummy);
        $this->assertSame($value, $collection);
    }

    public function testGetDataForViewSettings()
    {
        $item = $this->getMockBuilder(AccountManager::class)
            ->disableOriginalConstructor()
            ->setMethods(['getDataForSettingView'])
            ->getMock()
        ;

        $collection = $this->getMockBuilder(Collection::class)->getMock();

        $expectReturn = [
            'shippingDomestic' => $collection,
        ];

        $item->method('getDataForSettingView')
            ->with($this->isInstanceOf(EbayAccountInterface::class))
            ->will($this->returnValue([
                'shippingDomestic' => $collection,
            ]))
        ;

        $dummy = new AccountDummy();

        $value = $item->getDataForSettingView($dummy);
        $this->assertSame($value, $expectReturn);
    }

    public function testSameManagerInterface()
    {

        $item = $this->getMockBuilder(EbayShippingServiceInterface::class)
            ->getMock()
        ;

        $manager = new AccountManager($item);
        $this->assertTrue($manager instanceof AccountManagerInterface);

    }
}